/****************************************************************************
** Meta object code from reading C++ file 'kalkulacka.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../kalkulacka.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'kalkulacka.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_kalkulacka_t {
    QByteArrayData data[22];
    char stringdata0[145];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_kalkulacka_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_kalkulacka_t qt_meta_stringdata_kalkulacka = {
    {
QT_MOC_LITERAL(0, 0, 10), // "kalkulacka"
QT_MOC_LITERAL(1, 11, 5), // "klik1"
QT_MOC_LITERAL(2, 17, 0), // ""
QT_MOC_LITERAL(3, 18, 5), // "klik2"
QT_MOC_LITERAL(4, 24, 5), // "klik3"
QT_MOC_LITERAL(5, 30, 5), // "klik4"
QT_MOC_LITERAL(6, 36, 5), // "klik5"
QT_MOC_LITERAL(7, 42, 5), // "klik6"
QT_MOC_LITERAL(8, 48, 5), // "klik7"
QT_MOC_LITERAL(9, 54, 5), // "klik8"
QT_MOC_LITERAL(10, 60, 5), // "klik9"
QT_MOC_LITERAL(11, 66, 5), // "klik0"
QT_MOC_LITERAL(12, 72, 4), // "zmaz"
QT_MOC_LITERAL(13, 77, 5), // "bodka"
QT_MOC_LITERAL(14, 83, 5), // "sucet"
QT_MOC_LITERAL(15, 89, 9), // "odcitanie"
QT_MOC_LITERAL(16, 99, 4), // "krat"
QT_MOC_LITERAL(17, 104, 6), // "deleno"
QT_MOC_LITERAL(18, 111, 5), // "rovne"
QT_MOC_LITERAL(19, 117, 7), // "nadruhu"
QT_MOC_LITERAL(20, 125, 9), // "odmocnina"
QT_MOC_LITERAL(21, 135, 9) // "plusminus"

    },
    "kalkulacka\0klik1\0\0klik2\0klik3\0klik4\0"
    "klik5\0klik6\0klik7\0klik8\0klik9\0klik0\0"
    "zmaz\0bodka\0sucet\0odcitanie\0krat\0deleno\0"
    "rovne\0nadruhu\0odmocnina\0plusminus"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_kalkulacka[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  114,    2, 0x08 /* Private */,
       3,    0,  115,    2, 0x08 /* Private */,
       4,    0,  116,    2, 0x08 /* Private */,
       5,    0,  117,    2, 0x08 /* Private */,
       6,    0,  118,    2, 0x08 /* Private */,
       7,    0,  119,    2, 0x08 /* Private */,
       8,    0,  120,    2, 0x08 /* Private */,
       9,    0,  121,    2, 0x08 /* Private */,
      10,    0,  122,    2, 0x08 /* Private */,
      11,    0,  123,    2, 0x08 /* Private */,
      12,    0,  124,    2, 0x08 /* Private */,
      13,    0,  125,    2, 0x08 /* Private */,
      14,    0,  126,    2, 0x08 /* Private */,
      15,    0,  127,    2, 0x08 /* Private */,
      16,    0,  128,    2, 0x08 /* Private */,
      17,    0,  129,    2, 0x08 /* Private */,
      18,    0,  130,    2, 0x08 /* Private */,
      19,    0,  131,    2, 0x08 /* Private */,
      20,    0,  132,    2, 0x08 /* Private */,
      21,    0,  133,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void kalkulacka::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        kalkulacka *_t = static_cast<kalkulacka *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->klik1(); break;
        case 1: _t->klik2(); break;
        case 2: _t->klik3(); break;
        case 3: _t->klik4(); break;
        case 4: _t->klik5(); break;
        case 5: _t->klik6(); break;
        case 6: _t->klik7(); break;
        case 7: _t->klik8(); break;
        case 8: _t->klik9(); break;
        case 9: _t->klik0(); break;
        case 10: _t->zmaz(); break;
        case 11: _t->bodka(); break;
        case 12: _t->sucet(); break;
        case 13: _t->odcitanie(); break;
        case 14: _t->krat(); break;
        case 15: _t->deleno(); break;
        case 16: _t->rovne(); break;
        case 17: _t->nadruhu(); break;
        case 18: _t->odmocnina(); break;
        case 19: _t->plusminus(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject kalkulacka::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_kalkulacka.data,
      qt_meta_data_kalkulacka,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *kalkulacka::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *kalkulacka::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_kalkulacka.stringdata0))
        return static_cast<void*>(const_cast< kalkulacka*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int kalkulacka::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 20;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
