/********************************************************************************
** Form generated from reading UI file 'kalkulacka.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KALKULACKA_H
#define UI_KALKULACKA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_kalkulackaClass
{
public:
    QAction *actionExit;
    QWidget *centralWidget;
    QPushButton *number7;
    QPushButton *number4;
    QPushButton *delenie;
    QPushButton *number2;
    QPushButton *number1;
    QPushButton *number8;
    QPushButton *number3;
    QPushButton *number9;
    QPushButton *number5;
    QPushButton *number6;
    QPushButton *nasobenie;
    QPushButton *odcitanie;
    QPushButton *pushButton_13;
    QPushButton *button_sqrt;
    QPushButton *button_nadruhu;
    QPushButton *button_clear;
    QPushButton *bodka;
    QPushButton *number0;
    QPushButton *rovne;
    QPushButton *sucet;
    QLineEdit *lineEdit;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *kalkulackaClass)
    {
        if (kalkulackaClass->objectName().isEmpty())
            kalkulackaClass->setObjectName(QStringLiteral("kalkulackaClass"));
        kalkulackaClass->resize(311, 369);
        actionExit = new QAction(kalkulackaClass);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        centralWidget = new QWidget(kalkulackaClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        number7 = new QPushButton(centralWidget);
        number7->setObjectName(QStringLiteral("number7"));
        number7->setGeometry(QRect(10, 70, 51, 51));
        number4 = new QPushButton(centralWidget);
        number4->setObjectName(QStringLiteral("number4"));
        number4->setGeometry(QRect(10, 130, 51, 51));
        delenie = new QPushButton(centralWidget);
        delenie->setObjectName(QStringLiteral("delenie"));
        delenie->setGeometry(QRect(190, 70, 51, 51));
        number2 = new QPushButton(centralWidget);
        number2->setObjectName(QStringLiteral("number2"));
        number2->setGeometry(QRect(70, 190, 51, 51));
        number1 = new QPushButton(centralWidget);
        number1->setObjectName(QStringLiteral("number1"));
        number1->setGeometry(QRect(10, 190, 51, 51));
        number8 = new QPushButton(centralWidget);
        number8->setObjectName(QStringLiteral("number8"));
        number8->setGeometry(QRect(70, 70, 51, 51));
        number3 = new QPushButton(centralWidget);
        number3->setObjectName(QStringLiteral("number3"));
        number3->setGeometry(QRect(130, 190, 51, 51));
        number9 = new QPushButton(centralWidget);
        number9->setObjectName(QStringLiteral("number9"));
        number9->setGeometry(QRect(130, 70, 51, 51));
        number5 = new QPushButton(centralWidget);
        number5->setObjectName(QStringLiteral("number5"));
        number5->setGeometry(QRect(70, 130, 51, 51));
        number6 = new QPushButton(centralWidget);
        number6->setObjectName(QStringLiteral("number6"));
        number6->setGeometry(QRect(130, 130, 51, 51));
        nasobenie = new QPushButton(centralWidget);
        nasobenie->setObjectName(QStringLiteral("nasobenie"));
        nasobenie->setGeometry(QRect(190, 130, 51, 51));
        odcitanie = new QPushButton(centralWidget);
        odcitanie->setObjectName(QStringLiteral("odcitanie"));
        odcitanie->setGeometry(QRect(190, 190, 51, 51));
        pushButton_13 = new QPushButton(centralWidget);
        pushButton_13->setObjectName(QStringLiteral("pushButton_13"));
        pushButton_13->setGeometry(QRect(250, 190, 51, 51));
        button_sqrt = new QPushButton(centralWidget);
        button_sqrt->setObjectName(QStringLiteral("button_sqrt"));
        button_sqrt->setGeometry(QRect(250, 130, 51, 51));
        button_nadruhu = new QPushButton(centralWidget);
        button_nadruhu->setObjectName(QStringLiteral("button_nadruhu"));
        button_nadruhu->setGeometry(QRect(250, 70, 51, 51));
        button_clear = new QPushButton(centralWidget);
        button_clear->setObjectName(QStringLiteral("button_clear"));
        button_clear->setGeometry(QRect(130, 250, 51, 51));
        bodka = new QPushButton(centralWidget);
        bodka->setObjectName(QStringLiteral("bodka"));
        bodka->setGeometry(QRect(70, 250, 51, 51));
        number0 = new QPushButton(centralWidget);
        number0->setObjectName(QStringLiteral("number0"));
        number0->setGeometry(QRect(10, 250, 51, 51));
        rovne = new QPushButton(centralWidget);
        rovne->setObjectName(QStringLiteral("rovne"));
        rovne->setGeometry(QRect(250, 250, 51, 51));
        sucet = new QPushButton(centralWidget);
        sucet->setObjectName(QStringLiteral("sucet"));
        sucet->setGeometry(QRect(190, 250, 51, 51));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(12, 10, 281, 51));
        lineEdit->setReadOnly(true);
        kalkulackaClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(kalkulackaClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 311, 26));
        kalkulackaClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(kalkulackaClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        kalkulackaClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(kalkulackaClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        kalkulackaClass->setStatusBar(statusBar);

        retranslateUi(kalkulackaClass);
        QObject::connect(number1, SIGNAL(clicked()), kalkulackaClass, SLOT(klik1()));
        QObject::connect(number2, SIGNAL(clicked()), kalkulackaClass, SLOT(klik2()));
        QObject::connect(number3, SIGNAL(clicked()), kalkulackaClass, SLOT(klik3()));
        QObject::connect(number4, SIGNAL(clicked()), kalkulackaClass, SLOT(klik4()));
        QObject::connect(number5, SIGNAL(clicked()), kalkulackaClass, SLOT(klik5()));
        QObject::connect(number6, SIGNAL(clicked()), kalkulackaClass, SLOT(klik6()));
        QObject::connect(number7, SIGNAL(clicked()), kalkulackaClass, SLOT(klik7()));
        QObject::connect(number8, SIGNAL(clicked()), kalkulackaClass, SLOT(klik8()));
        QObject::connect(number9, SIGNAL(clicked()), kalkulackaClass, SLOT(klik9()));
        QObject::connect(number0, SIGNAL(clicked()), kalkulackaClass, SLOT(klik0()));
        QObject::connect(button_clear, SIGNAL(clicked()), kalkulackaClass, SLOT(zmaz()));
        QObject::connect(bodka, SIGNAL(clicked()), kalkulackaClass, SLOT(bodka()));
        QObject::connect(sucet, SIGNAL(clicked()), kalkulackaClass, SLOT(sucet()));
        QObject::connect(odcitanie, SIGNAL(clicked()), kalkulackaClass, SLOT(odcitanie()));
        QObject::connect(nasobenie, SIGNAL(clicked()), kalkulackaClass, SLOT(krat()));
        QObject::connect(delenie, SIGNAL(clicked()), kalkulackaClass, SLOT(deleno()));
        QObject::connect(rovne, SIGNAL(clicked()), kalkulackaClass, SLOT(rovne()));
        QObject::connect(button_nadruhu, SIGNAL(clicked()), kalkulackaClass, SLOT(nadruhu()));
        QObject::connect(button_sqrt, SIGNAL(clicked()), kalkulackaClass, SLOT(odmocnina()));
        QObject::connect(pushButton_13, SIGNAL(clicked()), kalkulackaClass, SLOT(plusminus()));

        QMetaObject::connectSlotsByName(kalkulackaClass);
    } // setupUi

    void retranslateUi(QMainWindow *kalkulackaClass)
    {
        kalkulackaClass->setWindowTitle(QApplication::translate("kalkulackaClass", "kalkulacka", 0));
        actionExit->setText(QApplication::translate("kalkulackaClass", "exit", 0));
        number7->setText(QApplication::translate("kalkulackaClass", "7", 0));
        number4->setText(QApplication::translate("kalkulackaClass", "4", 0));
        delenie->setText(QApplication::translate("kalkulackaClass", "/", 0));
        number2->setText(QApplication::translate("kalkulackaClass", "2", 0));
        number1->setText(QApplication::translate("kalkulackaClass", "1", 0));
        number8->setText(QApplication::translate("kalkulackaClass", "8", 0));
        number3->setText(QApplication::translate("kalkulackaClass", "3", 0));
        number9->setText(QApplication::translate("kalkulackaClass", "9", 0));
        number5->setText(QApplication::translate("kalkulackaClass", "5", 0));
        number6->setText(QApplication::translate("kalkulackaClass", "6", 0));
        nasobenie->setText(QApplication::translate("kalkulackaClass", "*", 0));
        odcitanie->setText(QApplication::translate("kalkulackaClass", "-", 0));
        pushButton_13->setText(QApplication::translate("kalkulackaClass", "+/-", 0));
        button_sqrt->setText(QApplication::translate("kalkulackaClass", "sqrt(x)", 0));
        button_nadruhu->setText(QApplication::translate("kalkulackaClass", "x^2", 0));
        button_clear->setText(QApplication::translate("kalkulackaClass", "C", 0));
        bodka->setText(QApplication::translate("kalkulackaClass", ".", 0));
        number0->setText(QApplication::translate("kalkulackaClass", "0", 0));
        rovne->setText(QApplication::translate("kalkulackaClass", "=", 0));
        sucet->setText(QApplication::translate("kalkulackaClass", "+", 0));
    } // retranslateUi

};

namespace Ui {
    class kalkulackaClass: public Ui_kalkulackaClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KALKULACKA_H
