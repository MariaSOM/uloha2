#ifndef KALKULACKA_H
#define KALKULACKA_H


#include <QtWidgets/QMainWindow>
#include "ui_kalkulacka.h"
#include <QLCDNumber>
#include <QTextStream>
#include <qlineedit.h>
#include <qsize.h>

class kalkulacka : public QMainWindow
{
	Q_OBJECT

public:
	kalkulacka(QWidget *parent = 0);
	~kalkulacka();

private:
	//bool zaporna = false;
	bool desatine = false;
	bool plus;
	bool minus;
	bool nasobenie;
	bool delenie;
	double a, b, c;
	Ui::kalkulackaClass ui;

private slots:
	void klik1();
	void klik2();
	void klik3();
	void klik4();
	void klik5();
	void klik6();
	void klik7();
	void klik8();
	void klik9();
	void klik0();
	void zmaz();
	void bodka();
	void sucet();
	void odcitanie();
	void krat();
	void deleno();
	void rovne();
	void nadruhu();
	void odmocnina();
	void plusminus();
};

#endif // KALKULACKA_H
