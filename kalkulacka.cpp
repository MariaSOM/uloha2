#include "kalkulacka.h"

kalkulacka::kalkulacka(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

kalkulacka::~kalkulacka()
{

}

void kalkulacka::klik1()
{
	ui.lineEdit->setText(ui.lineEdit->text() + "1" );
}


void kalkulacka::klik2()
{
	ui.lineEdit->setText(ui.lineEdit->text() + "2");
}

void kalkulacka::klik3()
{
	ui.lineEdit->setText(ui.lineEdit->text() + "3");
}

void kalkulacka::klik4()
{
	ui.lineEdit->setText(ui.lineEdit->text() + "4");
}

void kalkulacka::klik5()
{
	ui.lineEdit->setText(ui.lineEdit->text() + "5");
}

void kalkulacka::klik6()
{
	ui.lineEdit->setText(ui.lineEdit->text() + "6");
}

void kalkulacka::klik7()
{
	ui.lineEdit->setText(ui.lineEdit->text() + "7");
}

void kalkulacka::klik8()
{
	ui.lineEdit->setText(ui.lineEdit->text() + "8");
}

void kalkulacka::klik9()
{
	ui.lineEdit->setText(ui.lineEdit->text() + "9");
}

void kalkulacka::klik0()
{
	ui.lineEdit->setText(ui.lineEdit->text() + "0");
}

void kalkulacka::zmaz()
{
	ui.lineEdit->setText("");
}

void kalkulacka::bodka()
{
	if (desatine == false)
	{
		ui.lineEdit->setText(ui.lineEdit->text() + ".");
		desatine = true;
	}
}

void kalkulacka::sucet()
{
	a = ui.lineEdit->text().toDouble();
	plus = true;
	ui.lineEdit->setText("");
	desatine = false;
}

void kalkulacka::odcitanie()
{
	a = ui.lineEdit->text().toDouble();
	minus = true;
	ui.lineEdit->setText("");
	desatine = false;
}

void kalkulacka::krat()
{
	a = ui.lineEdit->text().toDouble();
	nasobenie = true;
	ui.lineEdit->setText("");
	desatine = false;
}

void kalkulacka::deleno()
{
	a = ui.lineEdit->text().toDouble();
	delenie = true;
	ui.lineEdit->setText("");
	desatine = false;
}

void kalkulacka::rovne()
{
	b = ui.lineEdit->text().toDouble();
	if (plus == true)
	{
		c = a + b;
		ui.lineEdit->setText(QString::number(c));
	}
	if (minus == true)
	{
		c = a - b;
		ui.lineEdit->setText(QString::number(c));
	}
	if (nasobenie == true)
	{
		c = a * b;
		ui.lineEdit->setText(QString::number(c));
	}
	if (delenie == true)
	{
		c = a / b;
		ui.lineEdit->setText(QString::number(c));
	}
	plus = minus = nasobenie = delenie = false;
	desatine = false;
}

void kalkulacka::nadruhu()
{
	a = ui.lineEdit->text().toDouble();
	c = a * a;
	ui.lineEdit->setText(QString::number(c));
	desatine = false;
}

void kalkulacka::odmocnina()
{
	a = ui.lineEdit->text().toDouble();
	c = sqrt(a);
	ui.lineEdit->setText(QString::number(c));
}

void kalkulacka::plusminus()
{
		a = ui.lineEdit->text().toDouble();
		c = (-1) * a; 
		ui.lineEdit->setText(QString::number(c));
}